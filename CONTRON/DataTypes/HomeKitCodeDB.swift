//
//  HomeKitCodeDB.swift
//  HomeKitCodeDB
//
//  Created by Hendrik Meyer on 13.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

class HomeKitCodeEntry: Codable, Identifiable, Hashable, ObservableObject {
    static func == (lhs: HomeKitCodeEntry, rhs: HomeKitCodeEntry) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(setupURL)
        hasher.combine(setupCode)
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case setupURL
        case setupCode
    }

    @Published public var id: String
    @Published public var isNotEmpty: Bool = false
    @Published public var setupURL: String = "" {
        didSet {
            isNotEmpty = setupURL != "" || setupCode != 0
        }
    }

    @Published public var setupCode: UInt = 0 {
        didSet {
            isNotEmpty = setupURL != "" || setupCode != 0
        }
    }

    init(_ codeIdentifier: String, setupURL: String, setupCode: UInt) {
        id = codeIdentifier
        self.setupURL = setupURL
        self.setupCode = setupCode
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        setupURL = try values.decode(String.self, forKey: .setupURL)
        setupCode = try values.decode(UInt.self, forKey: .setupCode)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(setupURL, forKey: .setupURL)
        try container.encode(setupCode, forKey: .setupCode)
    }

    public func reset() {
        setupURL = ""
        setupCode = 0
        try? HomeKitCodeDB.shared.save(entity: self)
    }
}

class HomeKitCodeDB: NSObject, ObservableObject {
    public static let shared = HomeKitCodeDB()
    @Published public var codes: [HomeKitCodeEntry] = []

    func findOrCreate(codeIdentifier: String, setupURL: String = "", setupCode: UInt = 0) -> HomeKitCodeEntry {
        guard let ret = codes.first(
            where: { entry in
                entry.id == codeIdentifier
            }
        ) else {
            return HomeKitCodeEntry(codeIdentifier, setupURL: setupURL, setupCode: setupCode)
        }

        return ret
    }

    override init() {
        let read: (content: Data?, location: StorageLocation) = FileIO.shared.readFromFile(
            fromFile: "HomeKitCodes",
            fileExtension: ".json",
            subDirectory: "HomeKitCodes"
        )
        guard let content = read.content else {
            print("no stored db")
            return
        }
        do {
            let dec = JSONDecoder()
            codes = try dec.decode([HomeKitCodeEntry].self, from: content)
            print("loaded")
        } catch {
            print(error)
        }
    }

    func save(entity: HomeKitCodeEntry? = nil) throws {
        if entity != nil {
            if !codes.contains(where: { input in
                input.id == entity!.id
            }) {
                codes.append(entity!)
            }
        }

        let je = JSONEncoder()
        je.outputFormatting = .prettyPrinted
        do {
            let json = try je.encode(codes)
            let loc = FileIO.shared.writeToFile(
                json,
                toFile: "HomeKitCodes",
                fileExtension: ".json",
                subDirectory: "HomeKitCodes"
            )
            guard loc != .undefined else {
                throw POSIXError(.EIO)
            }
        } catch {
            throw error
        }
    }
}
