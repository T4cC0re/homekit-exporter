//
//  HMCharJS.swift
//  HMCharJS
//
//  Created by Hendrik Meyer on 05.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import HomeKit
import JavaScriptCore

class HMCharJS: NSObject, IParameter, HMCharJSExports, ObservableObject {
    var defaultValue: Any = 0
    @Published public var value: Any
    @Published private(set) var valueLoaded: Bool = false
    public let uuid: String
    private let characteristic: HMCharacteristic
    @Published private(set) var friendlyName: String?
    @Published private(set) var displayName: String
    public let characteristicType: String
    @Published private(set) var isKnown: Bool
    public let step: Double?
    public let minVal: Double?
    public let maxVal: Double?
    @Published private(set) var units: String?
    public let validValues: [NSNumber]?
    @Published public var icon: String = "slider.horizontal.3"
    public let isReadable: Bool
    public let isWritable: Bool
    public let supportsNotifications: Bool
    public let isHidden: Bool
    @Published private(set) var format: String?
    public let serviceUUID: String
    public let accessoryUUID: String

    static func getOrInit(characteristic: HMCharacteristic) -> HMCharJS {
        guard let retrieved = HomeKitState.shared.getCharacteristic(uuid: characteristic.uniqueIdentifier.uuidString) else {
            return HMCharJS(characteristic: characteristic)
        }
        return retrieved
    }

    /**
     If we *need* to access the accessory, we can do that, but the access is expensive, as we need to find it first.
     */
    private var accessoryInstance_expensive: HMAccJS? {
        guard accessoryUUID != "" else {
            return nil
        }
        return HomeKitState.shared.getAccessory(uuid: accessoryUUID)
    }

    init(characteristic: HMCharacteristic) {
        uuid = characteristic.uniqueIdentifier.uuidString
        serviceUUID = characteristic.service?.uniqueIdentifier.uuidString ?? ""
        accessoryUUID = characteristic.service?.accessory?.uniqueIdentifier.uuidString ?? ""
        value = characteristic.value as Any
        self.characteristic = characteristic
        characteristicType = self.characteristic.characteristicType
        RegisterFriendlyNameIfExists(self.characteristic)
        friendlyName = FriendlyNameForTypeKey(characteristicType, showUnknown: false)
        step = self.characteristic.metadata?.stepValue?.doubleValue
        minVal = self.characteristic.metadata?.minimumValue?.doubleValue
        maxVal = self.characteristic.metadata?.maximumValue?.doubleValue
        units = self.characteristic.metadata?.units
        validValues = self.characteristic.metadata?.validValues
        isReadable = self.characteristic.properties.contains(HMCharacteristicPropertyReadable)
        isWritable = self.characteristic.properties.contains(HMCharacteristicPropertyWritable)
        supportsNotifications = self.characteristic.properties.contains(HMCharacteristicPropertySupportsEventNotification)
        // TODO: Signify event support in the icon.
        isHidden = self.characteristic.properties.contains(HMCharacteristicPropertyHidden)
        format = self.characteristic.metadata?.format
        displayName = characteristicType
        isKnown = false
        super.init()
        isKnown = friendlyName != nil
        if isKnown {
            displayName = friendlyName!
        }
        if !isReadable {
            valueLoaded = true
        }

        switch characteristicType {
        case HMCharacteristicTypeSerialNumber:
            print("HMCHarJS: HMCharacteristicTypeSerialNumber: \(value)")
        case HMCharacteristicTypeVersion:
            print("HMCHarJS: HMCharacteristicTypeVersion: \(value)")
        case HMCharacteristicTypeLabelIndex:
            print("HMCHarJS: HMCharacteristicTypeLabelIndex: \(value)")
        case HMCharacteristicTypeHardwareVersion:
            print("HMCHarJS: HMCharacteristicTypeHardwareVersion: \(value)")
        case HMCharacteristicTypeSoftwareVersion:
            print("HMCHarJS: HMCharacteristicTypeSoftwareVersion: \(value)")
        default:
//            self.characteristic.service?.accessory?.category.categoryType
            print("HMCHarJS: " + (self.characteristic.metadata?.manufacturerDescription ?? ""))
        }

        HomeKitState.shared.setCharacteristic(characteristic: self)
    }

    func localSetAndEnableNotifications(_ val: Any, _ closure: @escaping (Any?, Error?) -> Void) {
        Threading.uiThread.runSync {
            self.value = val
            self.valueLoaded = true
        }

        Threading.homeKitThread.runAsync {
            switch true {
            case self.supportsNotifications && !self.characteristic.isNotificationEnabled:
                // We *can* enable notifications and they are not already enabled.
                self.characteristic.enableNotification(true) { error in
                    if error != nil {
                        if (error as! HMError).errorCode == 4 {
                            print("Accessory unreachable")
                            closure(nil, error)
                        } else if (error as! HMError).errorCode == 7 {
                            // Notifications are unsupported. Ignore this
                            print("Notifications are unsupported")
                            closure(val, nil)
                        } else {
                            print(error as Any)
                            closure(nil, error)
                        }
                    } else {
                        print("Accessory enableNotification OK")
                        closure(val, nil)
                    }
                }
            case self.characteristic.isNotificationEnabled:
                // Notifications are enabled, we can call the callback immediately
                fallthrough
            default:
                // Notifications should not be enabled in this run. Call the callback
                closure(val, nil)
            }
        }
    }

    func setInHomeKit(_ val: Any, _ closure: @escaping (Any?, Error?) -> Void) {
        Threading.homeKitThread.runAsync {
            self.characteristic.writeValue(val) { error in
                if error != nil {
                    closure(nil, error)
                } else {
                    self.localSetAndEnableNotifications(val) { _val, error in
                        if error != nil {
                            closure(nil, error)
                        } else {
                            closure(_val, nil)
                        }
                    }
                }
            }
        }
    }

    public func refresh(updateOnly _: Bool) {
        Threading.uiThread.runSync { [self] in
            self.format = self.characteristic.metadata?.format
            self.units = self.characteristic.metadata?.units
            self.friendlyName = FriendlyNameForTypeKey(self.characteristicType, showUnknown: false)
            self.isKnown = self.friendlyName != nil
            if self.isKnown {
                self.displayName = self.friendlyName!
            } else {
                self.displayName = self.characteristicType
            }
        }

        characteristic.readValue { [self]
            error in
            if error != nil {
                if (error as! HMError).errorCode == 4 {
                    print("Accessory unreachable")
                    //                        err = error
                } else {
                    print(error as Any)
                    //                        err = error
                }
            } else {
                self.localSetAndEnableNotifications(characteristic.value as Any) { _, error in
                    if error != nil {
                        print("updateJSCharacteristicValue: error: \(error as Any)")
                    } else {
                        guard let svc = HomeKitState.shared.getService(uuid: self.serviceUUID) else {
                            // Data race on when clearing the service. Ignore this.
                            return
                        }
                        JavaScriptBridge.shared.pushUpdateToJS(service: svc, characteristic: characteristic)
                    }
                }
            }
        }
    }

    // MARK: JavaScript functions

    var getValue: (@convention(block) () -> JSValue)? {
        return { [unowned self] () in
            JSValue(newPromiseIn: JavaScriptBridge.shared.context, fromExecutor: { resolve, reject in
                Threading.jsThread.runAsync {
                    if !self.valueLoaded {
                        reject?.call(withArguments: [
                            JSValue(newErrorFromMessage: "Value is not loaded yet", in: JavaScriptBridge.shared.context)!,
                        ])
                    } else {
                        resolve?.call(withArguments: [self.value])
                    }
                }
            })
        }
    }

    var setValue: (@convention(block) (Any) -> JSValue)? {
        return { (_val: Any) in
            JSValue(newPromiseIn: JavaScriptBridge.shared.context, fromExecutor: { resolve, reject in
                Threading.jsThread.runAsync {
                    self.setInHomeKit(_val) { val, error in
                        if error != nil {
                            reject?.call(withArguments: ["\(error as Any)"])
                        } else {
                            resolve?.call(withArguments: [val as Any])
                        }
                    }
                }
            })
        }
    }
}
