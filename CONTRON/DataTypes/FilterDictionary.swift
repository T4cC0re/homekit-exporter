//
//  FilterDictionary.swift
//  FilterDictionary
//
//  Created by Hendrik Meyer on 06.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Combine
import Foundation
import SwiftUI

class FilterDictionary: ObservableObject {
    let didChange = PassthroughSubject<Void, Never>()

    var filters: [String: Bool] = [:] {
        didSet {
            didChange.send(())
        }
    }

    var keys: [String] {
        return Array(filters.keys)
    }

    func binding(for key: String) -> Binding<Bool> {
        return Binding<Bool>(
            get: {
                self.filters[key] ?? false
            },
            set: {
                self.filters[key] = $0
            }
        )
    }
}
