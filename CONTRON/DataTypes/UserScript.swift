//
//  UserScript.swift
//  UserScript
//
//  Created by Hendrik Meyer on 31.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import CryptoKit
import Foundation
import SwiftUI

enum EditState {
    case saved
    case unsaved
    case error
}

class UserScript: NSObject, ObservableObject {
    @Published private(set) var editState: EditState = .unsaved
    @Published private(set) var location: StorageLocation = .undefined
    static let template =
        """
        // HomeKit Extension Script
        /**
         * The above comment is required to be present at the very beginning of the
         * file. It is used to make sure we do not load anything other than an
         * extension script with a defined feature set.
         */

        /**
         * Load will be called once the script is loaded. Use this to register
         * parameters, load libraries, etc.
         *
         * Note: Anything involving state should be defined in userscript.init.
         */
        userscript.load = async () => {
          /*
           * We can use this to have a function to load a library from a website.
           */
          //await loadScript("https://example.com/extension.library.js");

          /*
           * We can register parameters to use in the script.
           * They can be defined with extra configuration for display of UI elements...
           */
          await registerParameter("target temperature", 22.5, {icon: 'thermometer', minVal: 16.0, maxVal: 35.0, displayName: 'Target Temperature ˚C', step: 0.5});
          /*
           * ... or use UI elements inferred by type (such as a toggle for this boolean)
           */
          await registerParameter("enable heating automation", false);

          /*
           * Finally we return a string to serve as a version number. This is required.
           */
          return "template";
        };

        /**
         * Init will be called after load.
         * The purpose of this function is to initialize everything you need in your script.
         * If your script manages state, make sure to reset the state here.
         */
        userscript.init = async () => {
        };

        /**
         * Tick is called based on an interval. You can use this to perform routine operations.
         */
        userscript.tick = async () => {
            console.log("tick tock");
        };
        """
    public let name: String
    @Published public var content: String {
        didSet {
            editState = .unsaved
        }
    }

    public let isLibrary: Bool
    @Published private(set) var isPlaceholder: Bool

    public static func deleteCachedLibraries() {
        FileIO.shared.deleteByExtension(fileExtension: ".library.js")
    }

    /**
     Initializes a user script by name. If there is a script with the given name it will be loaded, otherwise a new one will be created from the template string.
     (unless createIfNotExisting is false, then it will throw).
     Might throw also, if it cannot save.
     */
    init(_ name: String, createIfNotExisting: Bool) throws {
        isPlaceholder = false
        self.name = name
        isLibrary = false
        self.content = ""

        super.init()

        var content: (content: String?, location: StorageLocation)
        content = FileIO.shared.readFromFile(fromFile: self.name, fileExtension: ".userscript.js")
        if content.content != nil {
            self.content = content.content!
            location = content.location
            editState = .saved

            return
        } else {
            if createIfNotExisting {
                self.content = UserScript.template
                editState = .unsaved
                location = .undefined
                isPlaceholder = true
            } else {
                throw POSIXError(.ENOENT)
            }
        }
    }

    /**
     Initializes a user script, which is imported as a library.
     */
    init(asLibrary: String, withContent: String) throws {
        isPlaceholder = false
        name = asLibrary
        content = withContent
        isLibrary = true

        super.init()

        do {
            try save()
        } catch {
            throw error
        }
    }

    /**
     Initializes a user script, which is imported as a library.
     */
    init(libraryFromURL: URL) throws {
        isPlaceholder = false
        name = libraryFromURL.absoluteString
        self.content = ""
        isLibrary = true

        super.init()

        var content: (content: String?, location: StorageLocation)
        content = FileIO.shared.readFromFile(fromHash: name, fileExtension: ".library.js")
        if content.content != nil {
            self.content = content.content!
            location = content.location
            editState = .saved

            return
        }

        var err: Error?
        let sem = DispatchSemaphore(value: 0)
        UserScript.downloadScriptFromURL(url: libraryFromURL) { content, error in
            guard error == nil else {
                err = error!
                sem.signal()
                return
            }
            guard content != nil else {
                err = DownloadError(204)
                sem.signal()
                return
            }
            print("content: \(content!)")
            self.content = content!
            sem.signal()
        }

        sem.wait()
        guard err == nil else {
            // We throw the error, that might have been set in the closure handed to downloadScriptFromURL.
            throw err!
        }
        guard self.content != "" else {
            throw DownloadError(204)
        }

        do {
            try save()
        } catch {
            throw error
        }
    }

    public func save() throws {
        do {
            var location: StorageLocation = .undefined
            if isLibrary {
                location = FileIO.shared.writeToFile(content, toHash: name, fileExtension: ".library.js", forceLocation: self.location)
            } else {
                location = FileIO.shared.writeToFile(content, toFile: name, fileExtension: ".userscript.js", forceLocation: self.location)
            }
            if location == .undefined {
                throw POSIXError(.EIO)
            }
            self.location = location
            editState = .saved
            isPlaceholder = false
        } catch {
            editState = .error
            throw error
        }
    }

    public func delete() throws {
        if isLibrary {
            guard FileIO.shared.deleteFile(hash: name, fileExtension: ".library.js") else {
                throw POSIXError(.EIO)
            }
        } else {
            guard FileIO.shared.deleteFile(file: name, fileExtension: ".userscript.js") else {
                throw POSIXError(.EIO)
            }
            content = UserScript.template
            editState = .unsaved
            isPlaceholder = true
            location = .undefined
        }
    }

    private static func downloadScriptFromURL(url: URL, _ closure: @escaping (String?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.allowsCellularAccess = true
        request.allowsExpensiveNetworkAccess = true
        request.allowsConstrainedNetworkAccess = true
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.networkServiceType = .responsiveData

        let task = URLSession.shared.downloadTask(with: request) { localURL, response, error in
            guard error == nil else {
                return closure(nil, error)
            }
            guard let response = (response as? HTTPURLResponse) else {
                return closure(nil, DownloadError(0))
            }
            guard response.statusCode == 200 else {
                return closure(nil, DownloadError(response.statusCode))
            }
            guard let localURL = localURL else {
                return closure(nil, DownloadError(204))
            }

            if let string = try? String(contentsOf: localURL) {
                if !string.hasPrefix("// HomeKit Extension Script") {
                    return closure(nil, NoExtensionScriptError())
                } else {
                    return closure(string, nil)
                }
            } else {
                return closure(nil, DownloadError(204))
            }
        }
        task.resume()
    }
}
