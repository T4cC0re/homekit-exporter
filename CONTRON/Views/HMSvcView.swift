//
//  HMSvcView.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct HMSvcView: View {
    @ObservedObject var svc: HMSvcJS
    @AppStorage("show_unknown") var settingShowUnknown: Bool = UserDefaults.standard.bool(forKey: "show_unknown")

    var body: some View {
        if svc.svc.isPrimaryService {
            Label(svc.name, systemImage: "star.fill")
        } else {
            Label(svc.name, systemImage: "square.split.bottomrightquarter")
        }
        List {
            ForEach(Array(svc.characteristics.keys.sorted {
                svc.characteristics[$0]!.displayName < svc.characteristics[$1]!.displayName
            }), id: \.self) { key in
                HMCharView(characteristic: svc.characteristics[key]!)
            }
            if svc.characteristics.isEmpty {
                Label("No characteristics", systemImage: "")
            }
        }.onChange(of: svc.characteristics, perform: { _ in })
    }
}

struct HMSvcView_Previews: PreviewProvider {
    static var previews: some View {
        //        HMSvcView(s)
        Text("Mock")
    }
}
