# CONTRON

## What is CONTRON?

CONTRON is the best way to control your HomeKit enabled smart home.

With CONTRON you can control your home like never before. CONTRON enables you to tweak all settings, otherwise hidden in your HomeKit accessories.

The optional scripting engine, allows you to access various aspects of your home programatically without writing your own app from scratch.

## Configuring the app

On first launch, you will be asked to grant access to HomeKit. Once that is done, you can already start controlling all the different properties of your accessories.  
To enable editing of unofficial characteristics, enable it in the apps settings.

## Security

CONTRON on its own does not access the network (other than through HomeKit). However, user scripts can load external scripts from the internet and perform requests to HTTPS endpoints if the user so desires.  
While CONTRON only offers limited scripting support it is possible that including those untrusted/external resources could leave your home vulnerable.

## Privacy

CONTRON does not collect any personal information.

When using the optional user scripts feature, it is possible to establish interaction with external servers and/or services. Those external sservers and/or services might collect personal information such as the User Agent, IP address and other information.  
However, no such interaction is initiated without excplicit instruction from the user, by loading a remote script or issuing a request in a script.

## License

Copyright 2021 Hendrik 'T4cC0re' Meyer

GPLv3 (source) & official Apple App Store app (distribution)
